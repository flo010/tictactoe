import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;
public class TicTacToe {
        private static Scanner scanner = new Scanner(System.in);
        private static String input = "x";
        private static String Player1 = "x";
        private static String Player2 = "O";
        private Boolean GameState = true;
        private static int[][] PLayingField = {{0,0,0},{0,0,0},{0,0,0}};
        String x = "";


        public static void main(String args[]) {
                String Bye = "Thanks for plaing TicTacToe";
                TicTacToe Game= new TicTacToe();

                do {
                        Game.newboard();
                        Game.GameLogic();
                Game.GameState = newGame();
        }while (Game.GameState == true);
                System.out.println(Bye);
                return;


        }

        void GameInput(int PLayer) {
                System.out.println("For seting a Stone write 'x,y'");
                String Input = readInput();
                String[] Coordinates = Input.split(",", 2);
                PLayingField[Integer.parseInt(Coordinates[0])][Integer.parseInt(Coordinates[1])] = PLayer;
                System.out.println(Coordinates[0] + "  " + Coordinates[1]+ " " + PLayingField[0][1]);
                return;
        }

        void ConsoleOutput() {
                System.out.println("The Current PLayingstate:");
                System.out.println("");
                System.out.println("-------------------------------------------");
                System.out.println("|  " + PLayingField[0][0] + "  |  " + PLayingField[0][1] + "  |  " + PLayingField[0][2] + "  |");
                System.out.println("-------------------------------------------");
                System.out.println("|  " + PLayingField[1][0] + "  |  " + PLayingField[1][1] + "  |  " + PLayingField[1][2] + "  |");
                System.out.println("-------------------------------------------");
                System.out.println("|  " + PLayingField[2][0] + "  |  " + PLayingField[2][1] + "  |  " + PLayingField[2][2] + "  |");
                System.out.println("-------------------------------------------");
                return;
        }

        static String readInput() {
                input = scanner.nextLine();
                return input;
        }


        static void setPLayingState(int Row, int coulmn) {
                PLayingField[Row][coulmn] = 0;
                return;
        }

        void GameLogic(){
                String GameWon = "";
                Boolean loop = true;

                int CurrentPlayer = ThreadLocalRandom.current().nextInt(1, 2 + 1);
                System.out.println("Player "+ CurrentPlayer + "Starts.");
                do {
                        ConsoleOutput();
                        GameInput(CurrentPlayer);
                        if (Win(CurrentPlayer) == true) {
                                loop = false;
                                GameWon= "Game was won by Player" + CurrentPlayer ;

                        }
                        if (CurrentPlayer == 1) {
                                CurrentPlayer = 2;
                        } else if (CurrentPlayer == 2) {
                                CurrentPlayer = 1;
                        }
                }
                while(loop == true);
                System.out.println(GameWon);
                return;
                }



        boolean Win(int Player) {
                for (int i = 0; i < 3; i++) {

                                if (PLayingField[0][i] == Player && PLayingField[1][i] == Player && PLayingField[2][i] == Player) {
                                        return true;
                                }
                                if (PLayingField[i][0] == Player && PLayingField[i][1] == Player && PLayingField[i][2] == Player) {
                                        return true;
                                }


                }
                if (PLayingField[0][0] == Player && PLayingField[1][1] == Player && PLayingField[2][2] == Player) {
                        return true;
                }
                if (PLayingField[0][2] == Player && PLayingField[1][1] == Player && PLayingField[2][0] == Player) {
                        return true;
                }
                return false;
        }

        static boolean newGame(){
        Boolean Answer = true;
        Boolean loop = false;
                System.out.println("Do o want to start a new game? If yes type 'y', else type 'n'.");
                        do {
                                loop = false;
                                if (readInput() == "y") {

                                        for (int i = 0; i < 3; i++) {
                                                for (int j = 0; j < 3; j++) {
                                                        setPLayingState(i, j);
                                                }
                                        }
                                        return true;

                                } else if (readInput() == "n") {

                                        return false;
                                }
                                else
                                {
                                        System.out.println(" Type 'y' or 'n'.");
                                        loop=true;
                                }
                        }while(loop==true);
                        return true;
                }
        static void newboard()
        {
                for(int i=0;i<3;i++)
                {
                        for(int j=0;j<3;j++)
                        {
                                PLayingField[i][j] = 0;
                        }
                }

        }
}

